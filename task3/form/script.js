/*Задачи по теме Формы*/

/*==================================================================================*/

/*1)Создать форму, в которой есть поля: email, position, framework, message, agree
Поле position имеет тип radio, а поле framework тип checkbox, при этом, на поле типа
checkbox код должен обрабатывать несколько значений.*/

/*---------------------------------------------------------------------------------*/


/*2)При нажатии на кнопку submit cформировать объект на основе данных формы и
вывести в консоль JSON. */

const form = document.querySelector("#user-form");

const inputs = document.querySelectorAll("input, textarea");

const getCheckboxes = (name) => {
    return Array.from(form.querySelectorAll(`input[name='${name}']`))
        .filter(checkbox => checkbox.checked)
        .map(checkbox => checkbox.value);
}

form.addEventListener("submit", (e) => {
    e.preventDefault();

    const objectForm = Array.from(inputs).reduce((obj, field) => {
        obj[field.name] = field.type === "checkbox" ? getCheckboxes(field.name) : field.value;

        return obj;
    }, {});

    console.log(JSON.stringify(objectForm));
});


/*---------------------------------------------------------------------------------*/
/**3) Реализовать счетчик ввода символов Characters для текстовых полей. */

const textForm = document.querySelector("#user-name-input");
let out = document.querySelector(".char-counter-out");

textForm.oninput = () => out.textContent = textForm.value.length;



/*---------------------------------------------------------------------------------*/
/**4) Реализовать валидацию полей, чтобы все поля были заполнены, в противном случаине выводить в консоль JSON.*/




