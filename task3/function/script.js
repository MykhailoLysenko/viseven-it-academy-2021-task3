'use strict'
/*Задачи по теме Функции*/

/*==================================================================================*/

/*1) Реализовать функцию multiply которая принимает 3 числовых аргумента и умножает эти 
числа и выводит результат. Вывести можно с помощью console.log или alert*/

function multiply(a, b, c) {

    console.log(a * b * c);

}

multiply(3, 3, 3);

/*---------------------------------------------------------------------------------*/


/*2) Реализовать первую задачу в 4 вариантах функции */

//function declaration

function funcDecMultiply(a, b, c) {

    console.log(a * b * c);

}

//funcDecMultiply(2, 3, 4);

//function expression

const funcExpMultiply = function (a, b, c) {

    console.log(a * b * c);

}

funcExpMultiply(2, 2, 2);


//arrow function

const arrowFuncMultiplay = (a, b, c) => console.log(a * b * c);

//arrowFuncMultiplay(2, 2, 4);


//class Function

let classFuncMultiply = new Function('a', 'b', 'c', 'console.log(a * b * c)');

//classFuncMultiply(4, 4, 5);

/*---------------------------------------------------------------------------------*/
/**3)Есть код (на скрине), который создает таблицу умножения. Сейчас таблица умножения
создается только на 10. Чтобы создать к примеру на 15 или 20, нужно будет повторить
много раз один и ту же реализацию. Реализовать функцию generateMultiplicationTables,
которая будет принимать на вход один аргумент, который будет содержать в себе число,
какая будет таблица, на 10, 15, 20 и выводить результат на экран.*/

const generateMultiplicationTables = (a) => {

    let template = '<table><tr>';

    for (let x = 1; x <= a; x++) {
        for (let y = 1; y <= a; y++) {
            let el = `<td> ${x * y}</td>`;
            template += el;
        }
        template += '</tr>';
    }
    template += '</table>';

    return template;
}

document.querySelector('.out-3').innerHTML = generateMultiplicationTables(5);

/*---------------------------------------------------------------------------------*/
/**4) Сейчас наша функция может выводить только таблицу умножения. Модифицировать
функцию generateMultiplicationTables так, чтобы она могла генерировать таблицы и на
сложения и на вычитания, даже чтобы можно было возводить числа в степень, при этом
нельзя дублировать код.*/

const generateMultifunctionTables = (a, b) => {

    let template = '<table><tr>';

    for (let x = 0; x <= a; x++) {
        if (typeof b === 'undefined') {
            for (let y = 0; y <= a; y++) {
                let el = `<td>${x + y} | ${y - x}</td>`;
                template += el;
            }
        } else {
            for (let y = 1; y <= a; y++) {
                if (x == 0) {
                    continue;
                }
                else if (x == 1) {
                    let el = `<td>${y}</td>`;
                    template += el;
                }
                else {
                    let el = `<td>${Math.pow(x, y)}</td>`;
                    template += el;
                }
            }
        }
        template += '</tr>';
    }
    template += '</table>';

    return template;

}

//Привводе одногоаргумента генерируется таблица сложения и вычитания, а при добавлении второго аргумента - таблица степеней.

document.querySelector('.out-4').innerHTML = generateMultifunctionTables(5, 2);


/*---------------------------------------------------------------------------------*/
/**5) Реализовать функцию compose, которая на вход принимает функции, при этом каждая
функция возвращает результат другой функции, и другая функция возвращает результат
третьей функции, и так далее. Важно чтобы аргументов можно было передавать любое
количество.*/

const compose = (...arrFunc) => (currentFunc) => {

    if (arrFunc.length === 0) return currentFunc;

    let resultFunc = currentFunc;

    for (let i = 0; i < arrFunc.length; i++) {

        resultFunc = arrFunc[i](resultFunc);

    }

    return resultFunc;
}

const add = n => n + 1;
const multipl = n => n * 2;


const combinedFunction = compose(add, multipl);
console.log(combinedFunction(1));